<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width,initial-scale=1">
<title>GESTION DES ETAPES</title>
	
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css" /> 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.min.css" />
	
</head>
<body>
<header>
<div id="titre_accueil">
<h2>Ajout/modif dans la table etape </h2>
</div>
</header>
	<div class="container-fluid" >
		
		<form:form servletRelativeAction="/etape/valide" modelAttribute="etape" method="post">
			<form:input type="hidden" path="id" name="id"></form:input> 
			<div class="container-fluid" >
			<div class="form-group row">
				<label class="col-form-label col-lg-2" for="etape">Nom de l'étape</label>
				<div class="form-group col-lg-4">
						<form:input class="form-control" path="nom" name="nom" type="text"
							placeholder="Nouvelle etape" />
						
				</div>
				<form:errors class="col-form-label errors col-lg-5" path="nom" />		
			</div>
			<div class="form-group row">
				<label class="col-form-label col-lg-2" for="description">Description de l'étape</label>
				<div class="form-group col-lg-4">
						<form:input class="form-control" path="description" name="description" type="text"
							placeholder="Description de l'étape" />
						
				</div>
				<form:errors class="col-form-label errors col-lg-5" path="description" />		
			</div>
			</div>
			<div class="container-fluid">
				<div id="btn-valid-form">
					<form:button type="submit" class="col-lg-1 btn btn-success">Valider</form:button>
					
				</div>	
			</div>			
						
		</form:form>
		<h6>LISTE DES ETAPES EXISTANTES EN BASE DE DONNEES</h6>			
<table id="table-2" class="table table-striped table-sm">
   <thead>
      <tr>
         <th>ETAPE</th>
		<th>DESCRIPTION</th>       
      </tr>
   </thead>
   <tbody>
   		<c:forEach var="etape" items="${etapes}"> 
 		<tr>
         	<td><a href="<c:url value='/etape/form/${etape.id}'/>"><c:out value="${etape.nom}"/></a></td>
         	<td><c:out value="${etape.description}"/>
         	
         
      </tr>
      </c:forEach>
      
      
   </tbody>
  
</table>
		
	</div>
	
 	<footer id=statusbarre>
 	<%@ include file="statusbarre.jsp" %>
 	</footer> 
 	 
 	<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/scripts/longueur.js"></script>
</body>
</html>