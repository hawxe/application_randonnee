<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width,initial-scale=1">
<title>RANDO</title>
	
 	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css" /> 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/font-awesome/css/font-awesome.min.css" />
	
		

</head>
<body>
<header>
<div id="titre_accueil" class="container-fluid">
	
	<h1 class="display-6">PROJET RANDO</h1>
	
</div>
<%@ include file="navbarre.jsp" %> 
</header>
	<div class="container-fluid" >
		
	
		
	</div>
 	
 	<footer id=statusbarre>
 	<%@ include file="statusbarre.jsp" %>
 	</footer>
 	
 	<script src="${pageContext.request.contextPath}/resources/js/jquery-3.4.1.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/scripts.js"></script>
 	<script src="${pageContext.request.contextPath}/resources/js/scripts/accueil.js"></script>
</body>
</html>