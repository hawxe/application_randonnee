

function affiche(msg,style) {
			$("#status_message").empty().append(msg).addClass(style);
  			$("#status_message").fadeIn("slow").delay(2500).fadeOut("slow");
  			};

	
function requeteAjaxPost(url,data) {	

 $.ajax({
      	url: url,
		method:"POST",
		dataType : 'text',
		contentType:"application/json",
		data:JSON.stringify(data),
		success:function(data) {
			if(data=="ok") {
			affiche("Composant enregistré !!","status_message_ok");
			}else{
				if(data="nok") {
				affiche("Erreur database","status_message_nok");	
			}
		}
		}	
	 });
};	

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function  getValParam(param) {
	var valparam=getUrlParameter(param);
	return valparam;	
}	
  