var matiere_ok=false;
var angle_ok=false;
var diametre_ok=false;
var rayon_ok=false;
var emboitement_ok=false;
var data={ "nom" :getValParam("nom")};
var params = {};
var matiere;
var diametre;
var angle;
var rayon;
var emboitement;
var promise;

$(document).ready(function() {
	matiere=$('select#matiere-select').find("option:selected");
	if(matiere.val()!="") {
		matiere_ok=true;
		params.matiere=matiere.val();
		console.log(params);
		
	};
	diametre=$('select#diametre-select').find("option:selected");
	if(diametre.val()!="") {
		diametre_ok=true;
		params.diametre=diametre.val();
		console.log(params);
	};	
	angle=$('select#angle-select').find("option:selected");
	if(angle.val()!="") {	
		angle_ok=true;
		params.angle=angle.val();
		console.log(params);
	};
	rayon=$('select#rayon-select').find("option:selected");
	if(rayon.val()!="") {
		rayon_ok=true;
		params.rayon=rayon.val();
		console.log(params);
	};
	emboitement=$('select#emboitement-select').find("option:selected");
	if(emboitement.val()!="") {	
		emboitement_ok=true;
		params.emboitement=emboitement.val();
		console.log(params);
	};
	verif(); 
		
});
	



$('select#matiere-select').on("change",function() {
		
		matiere=$(this).find("option:selected");
		if(matiere.val()!="") {
			matiere_ok=true;
			params.matiere=matiere.val();
		}else {
			matiere_ok=false;
		}
		console.log(params);
		verif();
});


$('select#diametre-select').on("change",function() {
		diametre=$(this).find("option:selected");
		if(diametre.val()!="") {
			diametre_ok=true;
			params.diametre=diametre.val();
		}else {
			diametre_ok=false;
		}
		console.log(params);
		verif();
});


$('select#angle-select').on("change",function() {
		angle=$(this).find("option:selected");
		if(angle.val()!="") {
			angle_ok=true;
			params.angle=angle.val();
		}else {
			angle_ok=false;
		}
		console.log(params);
		verif();
});

$('select#rayon-select').on("change",function() {
		rayon=$(this).find("option:selected");
		
		if(rayon.val()!="") {
			rayon_ok=true;
			params.rayon=rayon.val();
		}else {
			rayon_ok=false;
		}
		console.log(params);
		verif();
});

$('select#emboitement-select').on("change",function() {
		emboitement=$(this).find("option:selected");
				
		if(emboitement.val()!="") {
			emboitement_ok=true;
			params.emboitement=emboitement.val();
		}else {
			emboitement_ok=false;
		}
		console.log(params);
		verif();
});

function verif() {
	if(matiere_ok && angle_ok && diametre_ok && rayon_ok && emboitement_ok) {
		$('#btn-valid-coude').attr('disabled',false).removeClass('btn-warning').addClass('btn-success'); 
	}else{
		$('#btn-valid-coude').attr('disabled',true).removeClass('btn-success').addClass('btn-warning');
	}
	
}  


$('#btn-valid-coude').on("click",function() {
		params=params;
		data.params=JSON.stringify(params);
		requeteAjaxPost("/gestion/ajax/composant/ajoute",data)
		
		
});

