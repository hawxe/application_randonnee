package com.rando.controleur;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.rando.modele.Etape;
import com.rando.service.EtapeService;

@Controller
@RequestMapping(path="/etape")
public class EtapeControleur {
	
	@Autowired
	private EtapeService etapeService;

	@GetMapping("/form")
	public String ajoute(Model model ,@ModelAttribute("etape") Etape etape) {
		model.addAttribute("etapes", etapeService.getEtapes());		
		return "etape";
	}
	
	@GetMapping("/form/{id}")
	public String modifie(@PathVariable("id") int id,Model model ,@ModelAttribute("etape") Etape etape) {
		model.addAttribute("etapes", etapeService.getEtapes());
		model.addAttribute("etape",etapeService.getEtape(id));
		System.out.println(id);
		
		return "etape";
	}
	
		
	@PostMapping("/valide")
	public String valide( Model model,
			@Valid @ModelAttribute("etape") Etape etape,
			BindingResult form,
			RedirectAttributes redirectAttributes,
			@RequestParam(required=false) int id) 
	{
		
		System.out.println("valide"+id);
		String nom=etape.getNom();
		String err1=etapeService.getUniqueNom(nom);
		if(err1!=null && id==0) {
			form.rejectValue("nom","longueur.notunique.erreur",err1);
			
		}
		String err2=etapeService.parseEtape(nom);
		if(err2!=null) {
			form.rejectValue("nom","longueur.vide.erreur",err2);
			
		}
				
		if(form.hasErrors()) {
			model.addAttribute("etapes", etapeService.getEtapes());
			return "etape";
		}else {
			if(id==0) {
			etapeService.ajouter(etape);
			return "redirect:/etape/form";
			} else {
				etapeService.modifie(etape,id);
				return "redirect:/etape/form";	
		}
	}
}
}	