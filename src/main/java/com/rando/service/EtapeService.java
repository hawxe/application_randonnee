package com.rando.service;

import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rando.dao.EtapeDao;
import com.rando.modele.Etape;



@Service
public class EtapeService {
	
		
	@Autowired 
	private EtapeDao etapeDao;
			
	public List<Etape> getEtapes()  {
		
		return etapeDao.getEtapes();
		
	}
	
	public String getUniqueNom(String etape) {
		String etape1=etape.trim();
		for(Etape etp:etapeDao.getEtapes()) {
			if(etp.getNom().equalsIgnoreCase(etape1)) return "Le nom de l'étape existe déja dans la table !";
	}
	return null;
	}
	
	public String parseEtape(String nom) {
		
			if(nom.isBlank()) return "Le nom ne peut pas être vide !";
		
		return null;
		
	}
	
	
	
	@Transactional
	public void ajouter(Etape etape ) {
			Etape etp = new Etape();
			etp.setNom(etape.getNom());
			etp.setDescription(etape.getDescription());
			etapeDao.ajouter(etape);
		
			
	}
	
	@Transactional
	public void modifie(Etape etape,int id) {
		etapeDao.modifier(etape,id);
							
	}
	
	
	public Etape getEtape(int id) {
		return etapeDao.getEtape(id);
	}
		
			
}
	
	
	

