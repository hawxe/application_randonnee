package com.rando.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import org.springframework.stereotype.Repository;



import com.rando.modele.Etape;



@Repository
public class EtapeDao {
	
	@PersistenceContext
	private EntityManager em;
		
	
	public List<Etape> getEtapes() {
		return em.createQuery("select e from Etape e order by e.nom", Etape.class).getResultList();
	}

	
	public void ajouter(Etape etape) {
		em.persist(etape);
	
			
	}
	
	public void modifier(Etape etape,int id) {
		Etape etp=new Etape();
		etp=em.find(Etape.class, id);
		etp.setNom(etape.getNom());
		etp.setDescription(etape.getDescription());
	}
	
	public Etape getEtape(int id) {
		return em.createQuery("select e from Etape e where e.id=:id",Etape.class).setParameter("id",id).getSingleResult();
		
	}
	
	
	
}
